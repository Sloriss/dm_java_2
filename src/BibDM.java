import java.util.Collections;
import java.util.List;
import java.util.ArrayList;
public class BibDM{

    /**
     * Ajoute deux entiers
     * @param a le premier entier à ajouter
     * @param b le deuxieme entier à ajouter
     * @return la somme des deux entiers
     */
    public static Integer plus(Integer a, Integer b){
        return a+b;
    }


    /**
     * Renvoie la valeur du plus petit élément d'une liste d'entiers
     * VOUS DEVEZ LA CODER SANS UTILISER COLLECTIONS.MIN (i.e. vous devez le faire avec un for)
     * @param liste
     * @return le plus petit élément de liste
     */
    public static Integer min(List<Integer> liste){
        Integer minimum = null;
        if (liste.size() != 0){
          minimum = liste.get(0);
          for (Integer i : liste){
            if (minimum > i){
              minimum = i;
            }
          }
        }
        return minimum;
    }
    
    
    /**
     * Teste si tous les élements d'une liste sont plus petits qu'une valeure donnée
     * @param valeur 
     * @param liste
     * @return true si tous les elements de liste sont plus grands que valeur.
     */
    public static<T extends Comparable<? super T>> boolean plusPetitQueTous( T valeur , List<T> liste){
        if (liste.isEmpty()){
            return true;
        }
        else{
            for (T elem : liste){
                if (valeur.compareTo(elem)>=0){
                    return false;
                }
            }
            return true;
        }
    }



    /**
     * Intersection de deux listes données par ordre croissant.
     * @param liste1 une liste triée
     * @param liste2 une liste triée
     * @return une liste triée avec les éléments communs à liste1 et liste2
     */
    public static <T extends Comparable<? super T>> List<T> intersection(List<T> liste1, List<T> liste2){
        List<T> liste = new ArrayList<>();
        if (liste1.size() != 0 && liste2.size() != 0 && liste1.get(liste1.size()-1).compareTo(liste2.get(0)) < 0){
            return liste;
            }
        for (T elem:liste1){
            if (liste2.contains(elem)){
                if (!liste.contains(elem)){
                    liste.add(elem);
                }
            }
        }
        return liste;
    }


    /**
     * Découpe un texte pour obtenir la liste des mots le composant. texte ne contient que des lettres de l'alphabet et des espaces.
     * @param texte une chaine de caractères
     * @return une liste de mots, correspondant aux mots de texte.
     */
    public static List<String> decoupe(String texte){
        List<String> liste = new ArrayList<>();
        if (texte.isEmpty())
            return liste;
        String chaine = "";
        for (int i = 0; i < texte.length(); i++){
            char caractere = texte.charAt(i);
            if (caractere != ' ')
                chaine += caractere;
            if (caractere == ' ' || i == texte.length()-1){
                if (chaine != "")
                    liste.add(chaine);
                    chaine = "";
            }
        }
        return liste;
    }



    /**
     * Renvoie le mot le plus présent dans un texte.
     * @param texte une chaine de caractères
     * @return le mot le plus présent dans le texte. En cas d'égalité, renvoyer le plus petit dans l'ordre alphabétique
     */

    public static String motMajoritaire(String texte){
        if (texte.length() == 0){
            return null;
        }
        List<String> liste = new ArrayList<>();
        liste = BibDM.decoupe(texte);
        int max = Collections.frequency(liste, liste.get(0));
        String chaine = liste.get(0);
        for (int i = 1; i<liste.size()-1; i++){
            if (Collections.frequency(liste, liste.get(i)) >= max){
                chaine = liste.get(i);
            }
        }
        return chaine;
    }
    
    /**
     * Permet de tester si une chaine est bien parenthesée
     * @param chaine une chaine de caractères composée de ( et de )
     * @return true si la chaine est bien parentjèsée et faux sinon. Par exemple ()) est mal parenthèsée et (())() est bien parenthèsée.
     */
    public static boolean bienParenthesee(String chaine){
        int cpt = 0;
        for (int i=0; i < chaine.length(); i++){
            char caractere = chaine.charAt(i);
            if (caractere == '(')
                cpt++;
            if (caractere == ')'){
                if (cpt == 0)
                    return false;
                cpt--;
            }
        }
        return cpt == 0;
    }

    
    /**
     * Permet de tester si une chaine est bien parenthesée
     * @param chaine une chaine de caractères composée de (, de  ), de [ et de ]
     * @return true si la chaine est bien parentjèsée et faux sinon. Par exemple ([)] est mal parenthèsée alors ue ([]) est bien parenthèsée.
     */
    public static boolean bienParentheseeCrochets(String chaine){
        String mot = "";
        for (int i=0; i < chaine.length(); i++){
            char caractere = chaine.charAt(i);
            if (caractere == '(')
                mot += ")";
            if (caractere == '[')
                mot += "]";
            if (caractere == ')' || caractere == ']'){
                if (mot.isEmpty() || mot.charAt(mot.length()-1) != caractere )
                    return false;
                mot = mot.substring(0, mot.length()-1);
            }
        }
        return mot.isEmpty();
    }



    /**
     * Recherche par dichtomie d'un élément dans une liste triée
     * @param liste, une liste triée d'entiers
     * @param valeur un entier
     * @return true si l'entier appartient à la liste.
     */
    public static boolean rechercheDichotomique(List<Integer> liste, Integer valeur){
    int inddebut;
    int indmilieu;
    int indfin;
        indfin = liste.size()-1;
        inddebut = 0;
        while (inddebut <= indfin){
            indmilieu = (inddebut + indfin)/2;
            int i = liste.get(indmilieu).compareTo(valeur);
            if (i == 0){
                return true;
            }
            if (i > 0){
                indfin = indmilieu - 1;
            }
            if (i < 0 ){
                inddebut = indmilieu + 1;
            }
        } 
        return false;
    }
}

